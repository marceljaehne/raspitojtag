Partlist

Exported from RaspiToJTAG.sch at 02.02.2024 21:41

EAGLE Version 9.6.2 Copyright (c) 1988-2020 Autodesk, Inc.

Assembly variant: 

Part         Value          Device     Package  Library  Sheet

JP2          JTAG           PINHD-2X5  2X05     pinhead  1
JP3                         PINHD-1X3  1X03     pinhead  1
RASPBERRY1-3                PINHD-2X20 2X20     pinhead  1
